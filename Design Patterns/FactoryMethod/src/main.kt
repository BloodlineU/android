import org.w3c.dom.DocumentType

fun main () {

    class HTML(override var name: String) : Document{

        override fun createDocument()
        {
            println("document $name.html has been created")
        }

        override fun description(): String
        {
            return "HTML stands for Hyper Text Markup Language and is the standard markup language for Web pages."
        }

        override fun ext(): String
        {
            return ".html"
        }
    }

    class JavaScript(override  var name: String) : Document {

        override fun createDocument()
        {
            println("document $name.js has been created")
        }

        override fun description(): String
        {
            return "JavaScript is the programming language of HTML and the Web."
        }

        override fun ext(): String
        {
            return ".js"
        }
    }

    class Python(override  var name: String) : Document {

        override fun createDocument()
        {
            println("document $name.py has been created")
        }

        override fun description(): String
        {
            return "Python is a popular programming language. It was created by Guido van Rossum, and released in 1991."
        }

        override fun ext(): String
        {
            return ".py"
        }
    }

    class DocumentFactory(
        var documentType: String,
        var documentName: String
    ) {


        fun getDocument(): Document
        {
            when (documentType) {
                "HTML" -> return HTML(documentName)
                "JavaScript" -> return JavaScript(documentName)
                "Python" -> return Python(documentName)
                else -> throw Exception("Can't deal with $documentName.$documentType")
            }
        }
    }

    val pythonDoc = DocumentFactory("Python", "pythonTest").getDocument()
    pythonDoc.createDocument()

    val htmlDoc = DocumentFactory("HTML", "htmlTest").getDocument()
    htmlDoc.createDocument()

    val jsDoc = DocumentFactory("JavaScript","JSTest").getDocument()
    jsDoc.createDocument()
}

interface Document {
    var name: String

    fun createDocument()
    fun description(): String
    fun ext(): String
}