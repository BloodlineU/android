import kotlin.properties.Delegates

interface ValueChangedListener {
    fun onValueChanced (newValue: String)
}

class PrintTextChangedListener : ValueChangedListener {
    override fun onValueChanced(newValue: String) {
        println("Text is changed to: $newValue")
    }
}

class ObservableObject (listener: ValueChangedListener){
    var text: String by Delegates.observable(
        initialValue = "",
        onChange = {
            prop, old, new ->
            listener.onValueChanced(new)
        }
    )
}

fun main(args: Array<String>){
    val observableObject = ObservableObject(PrintTextChangedListener())
    observableObject.text = "Hello"
    observableObject.text = "There"
}