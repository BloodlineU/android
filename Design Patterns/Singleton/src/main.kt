object Singleton
{

    init {
        println("Singleton class invoked.")
    }
    var name = "Singleton 1"
    fun printName()
    {
        println(name)
    }

}

fun main(args: Array<String>) {
    Singleton.printName()
    Singleton.name = "Singleton 2"

    var a = A()
}

class A {

    init {
        println("Class init method. Singleton name property : ${Singleton.name}")
        Singleton.printName()
    }
}